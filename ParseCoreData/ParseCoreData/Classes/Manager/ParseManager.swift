//
//  ParseManager.swift
//  ParseCoreData
//
//  Created by Saurabh Jain on 11/20/15.
//  Copyright © 2015 Saurabh Jain. All rights reserved.
//

import UIKit

class ParseManager: NSObject {

    // Singleton
    private override init() {}
    static let sharedManager = ParseManager()
    
    // Fetch the employees
    func fetchEmployees(block: [PFEmployee]? -> ()) {
        
        let query = PFQuery(className: PFEmployee.parseClassName())
        query.findObjectsInBackgroundWithBlock { (objects, error) in
            
            // If we can get PFEmployee
            guard let employees = objects as? [PFEmployee] else {
                block(nil)
                return
            }
            
            if error != nil {
                block(nil)
            } else {
                block(employees)
            }
        }
    }
    
    /// Saves the PFEmployee to Parse
    func saveEmployee(employee: PFEmployee, block: ((Bool, NSError?) -> Void)?) {
        employee.saveInBackgroundWithBlock { (success, error) in
            block?(success, error)
        }
    }
    
    /// Searches employees
    func fetchEmployee(name: String?, age: NSNumber?, id: NSNumber?, salary: NSNumber?, completionBlock: ([PFEmployee]?) -> ()) {
        if let query = PFEmployee.query() {
            
            if let name = name {
                query.whereKey("fullName", equalTo: name)
            }
            
            if let age = age {
                query.whereKey("age", equalTo: age)
            }
            
            if let salary = salary {
                query.whereKey("salary", equalTo: salary)
            }
            
            if let id = id {
                query.whereKey("empID", equalTo: id)
            }
            
            query.findObjectsInBackgroundWithBlock { (objects, error) in
                completionBlock(objects as? [PFEmployee])
            }
        } else {
            completionBlock(nil)
        }
    }
}
