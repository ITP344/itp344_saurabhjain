//
//  Employee.swift
//  ParseCoreData
//
//  Created by Saurabh Jain on 11/20/15.
//  Copyright © 2015 Saurabh Jain. All rights reserved.
//

import Foundation
import CoreData


/// A subclass of NSManagedObject for storing data in Core Data
class Employee: NSManagedObject {

    @NSManaged var fullName: String?
    @NSManaged var age: NSNumber?
    @NSManaged var salary: NSNumber?
    @NSManaged var empID: NSNumber?
    
    // Entity name
    class func entityName() -> String {
        return "Employee"
    }
    
}

/// A subclass of PFObject for storing data on Parse
class PFEmployee: PFObject, PFSubclassing {
    
    @NSManaged var fullName: String?
    @NSManaged var age: NSNumber?
    @NSManaged var salary: NSNumber?
    @NSManaged var empID: NSNumber?

    class func parseClassName() -> String {
        return "Employee"
    }
    
}
