//
//  SearchViewController.swift
//  ParseCoreData
//
//  Created by Saurabh Jain on 11/26/15.
//  Copyright © 2015 Saurabh Jain. All rights reserved.
//

import UIKit
import CoreData

class SearchViewController: UIViewController {

    // IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var salaryTextField: UITextField!
    
    private var name: String? {
        get {
            if nameTextField.text?.characters.count == 0 {
                return nil
            }
            return nameTextField?.text
        }
    }
    
    private var age: NSNumber? { get { if let text = ageTextField?.text { return Int(text) }; return nil } }
    private var id: NSNumber? { get { if let text = idTextField?.text { return Int(text) }; return nil  } }
    private var salary: NSNumber? { get { if let text = salaryTextField?.text { return Int(text) }; return nil } }

    // Search from parse
    @IBAction func searchFromParseButtonClicked(sender: UIButton) {
        ParseManager.sharedManager.fetchEmployee(name, age: age, id: id, salary: salary) { objects in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let obj = objects {
                    self.performSegueWithIdentifier(Storyboard.searchSegue, sender: obj)
                }
            })
        }
    }
    
    // Search from core data
    @IBAction func searchFromCDButtonClicked(sender: UIButton) {
        let fetch = NSFetchRequest(entityName: Employee.entityName())
        
        var predicates = [NSPredicate]()
        
        if let name = name {
            predicates.append(NSPredicate(format: "fullName == %@", name))
        }
        
        if let age = age {
            predicates.append(NSPredicate(format: "age == %@", age))
        }
        
        if let salary = salary {
            predicates.append(NSPredicate(format: "salary == %@", salary))
        }
        
        if let id = id {
            predicates.append(NSPredicate(format: "empID == %@", id))
        }
        
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        fetch.predicate = compoundPredicate
        do {
            if let result = try moc?.executeFetchRequest(fetch) as? [Employee] {
                self.performSegueWithIdentifier(Storyboard.searchSegue, sender: result)
            }
        } catch {
            print("Could not execute fetch request")
        }
    }
    
    // Cancel
    @IBAction func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // Constants
    private struct Storyboard {
        static let searchSegue = "showResultsSegue"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Storyboard.searchSegue {
            if let vc = segue.destinationViewController as? HomeTableViewController {
                if let objects = sender as? [PFEmployee] {
                    vc.loadFromParse = true
                    vc.parseEmployees = objects
                } else if let objects = sender as? [Employee] {
                    vc.loadFromParse = false
                    vc.coreDataEmployees = objects
                }
            }
        }
    }
}
