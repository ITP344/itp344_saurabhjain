//
//  HomeTableViewController.swift
//  ParseCoreData
//
//  Created by Saurabh Jain on 11/20/15.
//  Copyright © 2015 Saurabh Jain. All rights reserved.
//

import UIKit
import CoreData

class HomeTableViewController: UITableViewController, AddViewControllerDelegate {

    // Data Source
    var parseEmployees = [PFEmployee]()
    var coreDataEmployees = [Employee]()
    
    // Load from Parse or Core Data
    var loadFromParse = false
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Basic
        tableView.registerNib(UINib(nibName: "EmployeeTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addEmployeeSegue" {
            if let vc = segue.destinationViewController as? AddViewController {
                vc.delegate = self
            }
        }
    }
    
    // MARK: - Add View Controller Delegate
    
    func addViewController(controller: AddViewController, didPressCancelButton sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func addViewController(controller: AddViewController, didSaveEmployee employee: PFEmployee) {
        parseEmployees.append(employee)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if loadFromParse {
            return parseEmployees.count
        } else {
            return coreDataEmployees.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as? EmployeeTableViewCell {
            if loadFromParse {
                cell.pfEmployee = parseEmployees[indexPath.row]
            } else {
                cell.cdEmployee = coreDataEmployees[indexPath.row]
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - IBActions
    
    @IBAction func loadFromParseButtonPressed(sender: UIButton) {
        loadFromParse = true
        print("Loading from Parse")
        fetchEmployeesFromParse()
    }
    
    @IBAction func loadFromCoreDataButtonPressed(sender: UIButton) {
        loadFromParse = false
        print("Loading from Core Data")
        let employeesFetch = NSFetchRequest(entityName: "Employee")
        do {
            if let fetchedEmployees = try moc?.executeFetchRequest(employeesFetch) as? [Employee] {
                coreDataEmployees = fetchedEmployees
                self.tableView.reloadData()
            }
            
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
    }
    
    // MARK: - Helper
    
    private func fetchEmployeesFromParse() {
        ParseManager.sharedManager.fetchEmployees { (employees) in
            guard let employees = employees else {
                return
            }
            
            if employees.count > 0 {
                self.parseEmployees = employees
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        }
    }
}


