//
//  AddViewController.swift
//  ParseCoreData
//
//  Created by Saurabh Jain on 11/21/15.
//  Copyright © 2015 Saurabh Jain. All rights reserved.
//

import UIKit
import CoreData

@objc protocol AddViewControllerDelegate: NSObjectProtocol {
    optional func addViewController(controller: AddViewController, didSaveEmployee employee: PFEmployee)
    optional func addViewController(controller: AddViewController, didPressCancelButton sender: UIButton)
}

class AddViewController: UIViewController {

    // Delegate
    weak var delegate: AddViewControllerDelegate?
    
    // IBOutlet
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var empidTextField: UITextField!
    @IBOutlet weak var salaryTextField: UITextField!
    
    // IBAction
    /// Save the employee to Parse and iCloud
    @IBAction func saveButtonClicked(sender: UIButton) {
        
        // Core data
        saveToCoreData()
        
        // Parse
        saveToParse { (employee, success, error) -> Void in
            
            // Print to console, if save to Parse
            if success {
                print("Saved to Parse")
            }
            
            // Call the delegate
            dispatch_async(dispatch_get_main_queue()) {
                self.delegate?.addViewController?(self, didSaveEmployee: employee)
            }
        }
        
    }
    
    // Cancel
    @IBAction func cancelButtonPressed(sender: UIButton) {
        delegate?.addViewController?(self, didPressCancelButton: sender)
    }
    
    // MARK: - Helper Functions
        
    private struct Constants {
        static let EntityEmployee = "Employee"
    }
    
    private func saveToParse(block: ((PFEmployee, Bool, NSError?) -> Void)? = nil) {
        // Parse
        let employee = PFEmployee()
        employee.fullName = nameTextField.text
        employee.age =  Int(ageTextField.text ?? "0")
        employee.empID = Int(empidTextField.text ?? "0")
        employee.salary = Int(salaryTextField.text ?? "0")
        
        // Save
        ParseManager.sharedManager.saveEmployee(employee) { (success, error) -> Void in
            block?(employee, success, error)
        }
    }
    
    private func saveToCoreData() {
        
        // Make sure that MOC is init
        guard let moc = moc else {
            return
        }
        
        // Entity
        if let entity = NSEntityDescription.entityForName(Constants.EntityEmployee, inManagedObjectContext: moc) {
            
            // Core Data Object: Employee
            let empl = Employee(entity: entity, insertIntoManagedObjectContext: moc)
            empl.fullName = nameTextField.text
            empl.age =  Int(ageTextField.text ?? "0")
            empl.empID = Int(empidTextField.text ?? "0")
            empl.salary = Int(salaryTextField.text ?? "0")
            
            // Save
            if moc.hasChanges {
                do {
                    try moc.save()
                    print("Added to Core Data")
                } catch {
                    print("Error saving")
                }
            }
        }
    }

}
