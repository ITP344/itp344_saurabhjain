//
//  EmployeeTableViewCell.swift
//  ParseCoreData
//
//  Created by Saurabh Jain on 11/20/15.
//  Copyright © 2015 Saurabh Jain. All rights reserved.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {

    /// Parse Employee
    var pfEmployee: PFEmployee? {
        didSet {
            updatePFGUI()
        }
    }
    
    /// Core Data Employee
    var cdEmployee: Employee? {
        didSet {
            updateCDGUI()
        }
    }

    // IBOutlets
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var empIdField: UILabel!
    @IBOutlet weak var ageField: UILabel!
    @IBOutlet weak var salaryField: UILabel!
    
    /// Updates GUI
    private func updatePFGUI() {
        guard let emp = pfEmployee else {
            return
        }
        
        nameField?.text = emp.fullName
        
        if let id = emp.empID {
            empIdField?.text = "Employee ID: \(id)"
        }
        
        if let age = emp.age {
            ageField?.text = "Age: \(age)"
        }
        
        if let salary = emp.salary {
            salaryField?.text = "Salary: \(salary)"
        }
    }
    
    private func updateCDGUI() {
        guard let emp = cdEmployee else {
            return
        }
        
        nameField?.text = emp.fullName
        
        if let id = emp.empID {
            empIdField?.text = "Employee ID: \(id)"
        }
        
        if let age = emp.age {
            ageField?.text = "Age: \(age)"
        }
        
        if let salary = emp.salary {
            salaryField?.text = "Salary: \(salary)"
        }
    }

}
